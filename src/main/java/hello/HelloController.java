package hello;

import org.joda.time.LocalTime;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by philip on 29.03.2017.
 */
@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {

        LocalTime currentTime = new LocalTime();

        return String.format("Hallo aus Aachen! Es ist %s.", currentTime);
    }
}
